var todayDate = new Date();
var month = todayDate.getMonth() + 1;
var day = todayDate.getDate() - 1;
if (day < 10)
    day = "0" + day;
var todayDateISO;
if (month < 10)
    todayDateISO = todayDate.getFullYear() + "-0" + month + "-" + day;
else
    todayDateISO = todayDate.getFullYear() + "-" + month + "-" + day;


let apiKey = "0b33tVGBkzuCWCTpuQCyyF2NhDxbVRu7kcsN9snr";

// calls other functions to display nasa daily digest
function displayDatePic() {
    let insert = '';
    let date;
    if ((date = sessionStorage.getItem("date")) != null)
        insert = "&date=" + date;

    let request = 'https://api.nasa.gov/planetary/apod?' + insert + '&api_key=' + apiKey;

    axios.get(request).then((response) => {
            let output = `
            <div class="well">
                <h4>${response.data.title}</h4>
                <p>${response.data.date}</p>
                <p>${response.data.explanation}</p>
            </div>
            `;

            $('#apodMH').css('background-image', `url(${response.data.hdurl})`);
            $('#apod').html(output);
        })
        .catch((err) => {
            console.log(err);
        });
}

function setDate() {
    let date = document.forms["searchform"]["fdate"].value;
    sessionStorage.setItem("date", date);
}

function resetDate() {
    sessionStorage.removeItem("date");
}

// Mars rover stuff
function displayMarsRover() {
    let date;
    if ((date = sessionStorage.getItem("date")) == null) {
        date = todayDateISO;
    }

    let dateDiv = document.querySelector("#datediv");
    dateDiv.innerHTML = date;

    axios.get('https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?earth_date=' + date + '&api_key=' + apiKey).then((response) => {
            let output = '';
            if (response.data.photos.length == 0) {
                output = `<h5 class="mx-auto">No photos uploaded today. Try another date.</h5>`;
            } else {
                let photos = response.data.photos;
                $.each(photos, (index, photo) => {
                    output += `
                    <div class="col-md-3">
                        <div class="well text-center">
                            <a href="${photo.img_src}" target=_blank"><img src="${photo.img_src}"></a>
                            <h5>${photo.rover.name}</h5>
                            <p>${photo.camera.full_name}</p>
                        </div>
                    </div>
                `;
                });
            }

            $('#rover').html(output);
        })
        .catch((err) => {
            console.log(err);
        });
}

//NEOs
function displayNeos() {
    let hdrDiv = document.querySelector("#hdrdiv");

    let date;
    if ((date = sessionStorage.getItem("date")) == null) {
        let todayDate = new Date();
        var month = todayDate.getMonth() + 1;
        var day = todayDate.getDate();
        if (day < 10)
            day = "0" + day;
        var todayDateISO;
        if (month < 10)
            todayDateISO = todayDate.getFullYear() + "-0" + month + "-" + day;
        else
            todayDateISO = todayDate.getFullYear() + "-" + month + "-" + day;

        date = todayDateISO;
    }

    let request = 'https://api.nasa.gov/neo/rest/v1/feed?start_date=' + date + '&end_date=' + date + '&api_key=' + apiKey;

    axios.get(request)
        .then((response) => {
            console.log(response);
            let output = '';
            if (response.data.near_earth_objects.length == 0) {
                hdrDiv.innerHTML = `We're safe... for today.`;
            } else {
                hdrDiv.innerHTML = `${response.data.element_count} asteroids threatening our existence today!`;

                let neos = response.data.near_earth_objects;
                let neoArr = neos[date];

                output = '<tr><th>Name</th><th>Size (Diameter)</th><th>Velocity</th><th>Magnitude</th><th>Closest Proximity</th><th>Nasa JPL Link</th></tr>'

                $.each(neoArr, (index, neo) => {
                    output += `
                    <tr><td>${neo.name}</td><td>${neo.estimated_diameter['meters']['estimated_diameter_max']} meters</td><td>${neo.close_approach_data[0]['relative_velocity']['miles_per_hour']} MPH</td><td>${neo.absolute_magnitude_h}</td><td>${neo.close_approach_data[0]['miss_distance']['miles']} miles</td><td><button class="btn btn-secondary"><a href="${neo.nasa_jpl_url}" target="_blank">Nasa JPL Info</a></button></td></tr>
                    `;

                });
            }
            let neoTable = document.querySelector("#neo");
            neoTable.innerHTML = output;
        })
        .catch((err) => {
            console.log(err);
        });
}
